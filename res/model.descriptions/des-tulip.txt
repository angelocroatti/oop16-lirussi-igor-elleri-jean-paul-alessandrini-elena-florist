I tulipani sono tra i fiori pi� riconoscibili al mondo. Il loro inconfondibile aspetto rosso a forma di calice composto da petali sovrapposti si presenta in una variet� di forme e dimensioni incredibili.
Sono il fiore principale dell�Olanda, nonch� il loro prodotto pi� esportato. Sono piante facilmente adattabili e semplici da coltivare, e dunque ideali per il giardino.
I tulipani sono di solito associati al romanticismo o alla creativit� e all�immaginazione per via dei loro colori prorompenti.
