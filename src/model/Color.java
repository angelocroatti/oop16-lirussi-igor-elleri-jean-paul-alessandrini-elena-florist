package model;

/**
 * color of flower.
 *
 */
public enum Color {

    /**
     * red of rose and tulip.
     */
    RED,

    /**
     * pink of carnation.
     */
    PINK,

    /**
     * green of generic plant.
     */
    GREEN,

    /**
     * orange of lily.
     */
    ORANGE,

    /**
     * blue of gentian.
     */
    BLUE,

    /**
     * white of daisy.
     */
    WHITE,

    /**
     * yellow of narcissus and sunflower.
     */
    YELLOW,

    /**
     * purple of orchid.
     */
    PURPLE;


}
