package view;
/**
 * ChoosingPanel interface.
 *
 */
public interface ChoosingPanel {
    /**
     * Sets border name of the panel.
     * @param name
     *              border to assign to the panel
     */
    void setBorderName(String name);
}